#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct  2 15:23:19 2023

@author: putta
"""

import pandas as pd
import plotly.express as px

curr_conv = pd.read_csv('CUR_DLR_INR.csv')

#curr_conv.head()

fig = px.line(curr_conv, x='DATE', y='RATE')
fig.show()