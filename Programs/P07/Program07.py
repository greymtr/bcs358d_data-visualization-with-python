import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

# # print(sns.get_dataset_names())

# #Distribution Plot
# crash_df = sns.load_dataset('planets')
# #sns.distplot(crash_df['not_distracted'], kde=False, bins=10)
# col_list = crash_df.columns.tolist()

def sinplot(n=10, flip=1):
    x = np.linspace(0, 14, 100)
    for i in range(1, n + 1):
        plt.plot(x, np.sin(x + i * .5) * (n + 2 - i) * flip)
        

        
sns.set_theme()
# sns.set_context("talk")
sns.set_context("notebook", font_scale=1.5, rc={"lines.linewidth": 2.5})

sinplot()
